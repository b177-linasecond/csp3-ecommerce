const express = require("express")
	  router = express.Router()
	  userController = require("../controllers/user")
	  auth = require("../auth")

// User registration
router.post("/register", (req, res) => {
	userController.userRegister(req.body).then(result => res.send(result))
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
})

router.post("/checkIfEmail", (req, res) => {
	userController.checkIfEmail(req.body).then(result => res.send(result))
})

router.get("/details", auth.verify, (req, res) => {
	let reqBody = auth.decode(req.headers.authorization)
	userController.getUserDetails({userId : reqBody.id}).then(result => res.send(result))
})

router.post("/checkPassword", (req, res) => {
	userController.checkPassword(req.body).then(result => res.send(result))
})

router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(result => res.send(result))
})

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let reqBody = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.setUserAsAdmin(req.params, reqBody).then(result => res.send(result))
})

router.get("/users", (req, res) => {
	userController.getAllUsers().then(result => res.send(result))
})

module.exports = router