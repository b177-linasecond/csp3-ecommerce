const User = require("../models/User")
	  bcrypt = require("bcrypt")
	  auth = require("../auth")

module.exports.userRegister = data => {
	let newUser = new User({
		firstName : data.firstName,
		lastName : data.lastName,
		mobileNo : data.mobileNo,
		email : data.email,
		password : bcrypt.hashSync(data.password,10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		}
		else {
			return true
		}
	})
}

module.exports.checkEmail = data => {
	return User.find({email : data.email}).then(result => {
		if(result.length > 0) {
			return false
		}
		else {
			return true
		}
	})
}

module.exports.checkIfEmail = data => {
	if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data.email)) {
		return Promise.resolve(true)
	}
	else {
		return Promise.resolve(false)
	}
}

module.exports.getUserDetails = data => {
	return User.findById(data.userId).then(result => {
		result.password = ""

		return result

	})

}

module.exports.checkPassword = data => {
	return User.findOne({email : data.email}).then(result => {
		if(result) {
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

			if(isPasswordCorrect) {
				return true
			}
			else {
				return false
			}
		}
		else {
			return false
		}

	})
}

module.exports.userLogin = data => {
	return  User.findOne({email : data.email}).then(result => {
		return { access : auth.createAccessToken(result) }
	})
};

module.exports.setUserAsAdmin = (reqParams, reqBody) => {
	if(reqBody.isAdmin) {

		return User.findById(reqParams.userId).then(result => {
			if(result.isAdmin) {
				return (`User is already an admin.`)
			}
			else {
				let userToAdmin = {
					isAdmin : true
				}

				return User.findByIdAndUpdate(reqParams.userId, userToAdmin).then((admin, error) => {
					if(error) {
						return false
					}
					else {
						return (`User is now an admin.`)
					}
				})
			}
		})
		
	}
	else {
		return Promise.resolve(`User is not authorized to assign an administrator.`)
	}
}

module.exports.getAllUsers = () => {
	return User.find({}).then(result => result)
}